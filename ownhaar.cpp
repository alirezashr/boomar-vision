#include <math.h>

// Qt includes

#include <QImage>
#include <QMap>
#include <QString>
#include <QDir>
#include <QFileInfoList>

// Local includes

#include "ownhaar.h"


Haar::SignatureData calculateSignature(const QImage& qImage) 
{

    // I userd -> cause i though new return a pointer and accessing pointer method is with -> as opposed with a dot.
    // Haar::ImageData* imageData = new Haar::ImageData;
    Haar::ImageData imageData;
    // imageData -> fillPixelData(qImage);
    imageData.fillPixelData(qImage);

    Haar::Calculator haar;
    haar.transform(&imageData);

    Haar::SignatureData sig;
    haar.calcHaar(&imageData, &sig);
    // we could use signatureAsText
    return sig;
    
}

double calculatePairScore(const Haar::SignatureData& querySig, const Haar::SignatureData& targetSig) 
{      

    // The table of constant weight factors applied to each channel and the weight bin

    //Haar::Weights weights((Haar::Weights::SketchType)type);
    Haar::Weights weights((Haar::Weights::SketchType)0);

    // layout the query signature for fast lookup

    Haar::SignatureMap queryMapY, queryMapI, queryMapQ;
    queryMapY.fill(querySig.sig[0]);
    queryMapI.fill(querySig.sig[1]);
    queryMapQ.fill(querySig.sig[2]);
    std::reference_wrapper<Haar::SignatureMap> queryMaps[3] = { queryMapY, queryMapI, queryMapQ };

    // Map imageid -> score. Lowest score is best.
    // any newly inserted value will be initialized with a score of 0, as required

    // ===

    double score = 0.0;

    // Step 1: Initialize scores with average intensity values of all three channels

    for (int channel = 0 ; channel < 3 ; ++channel)
    {
        score += weights.weightForAverage(channel) * fabs(querySig.avg[channel] - targetSig.avg[channel]);
    }

    // Step 2: Decrease the score if query and target have significant coefficients in common
    

    const Haar::WeightBin weightBin;

    int x        = 0;

    for (int channel = 0 ; channel < 3 ; ++channel)
    {
        const Haar::SignatureMap& queryMap = queryMaps[channel];

        for (int coef = 0 ; coef < Haar::NumberOfCoefficients ; ++coef)
        {
            // x is a pixel index, either positive or negative, 0..16384

            x = targetSig.sig[channel][coef];

            // If x is a significant coefficient with the same sign in the query signature as well,
            // decrease the score (lower is better)
            // Note: both method calls called with x accept positive or negative values

            if ((queryMap)[x])
            {
                score -= weights.weight(weightBin.binAbs(x), channel);
            }
        }
    }

    return score;

}


QMap<QString, double> scoreFolder(QString directoryPath, QString referencePath) {
    QMap<QString, double> scores;

    // Load the reference image
    QImage referenceImage(referencePath);
    // ... Process the reference image as needed ...
    Haar::SignatureData referenceSig = calculateSignature(referenceImage);

    // Open the directory
    QDir directory(directoryPath);
    if (!directory.exists()) {
        // Handle error: directory not found
        return scores;
    }

    // Iterate over all images in the directory
    QFileInfoList fileList = directory.entryInfoList(QDir::Files);
    for (const QFileInfo &fileInfo : fileList) {
        QString filePath = fileInfo.absoluteFilePath();

        // Load and process each image in the directory
        QImage image(filePath);
        // ... Process the image ...
        Haar::SignatureData querySig = calculateSignature(image);

        // Calculate similarity score
        double score = calculatePairScore(querySig, referenceSig);

        // Store the score in the map
        scores.insert(filePath, score);
    }

    return scores;
}
