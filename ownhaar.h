// Qt includes

#include <QImage>
#include <QString>

// Local includes

#include "haar.h"

// ... Include other necessary headers, like those for image processing ...

Haar::SignatureData calculateSignature(const QImage& qImage);

double calculatePairScore(const Haar::SignatureData& querySig, const Haar::SignatureData& targetSig);

QMap<QString, double> scoreFolder(QString directoryPath, QString referencePath);
