// Local Includes
#include "ownhaar.h"
#include <iostream>

#include <QString>
#include <QMap>
// #include <QCoreApplication>

int main(int argc, char *argv[]) 
{
    // QCoreApplication app(argc, argv);
    if (argc < 3) {
        std::cerr << "Usage: " << argv[0] << " <directory_path> <reference_path>\n ";
        return 1;
    } 

    QString directoryPath = argv[1];
    QString referencePath = argv[2];

    QMap<QString, double> scores = scoreFolder(directoryPath, referencePath);
     // Iterate through the QMap and print the scores
    QMapIterator<QString, double> i(scores);
    while (i.hasNext()) {
        i.next();
        std::cout << "File: " << i.key().toStdString() << " has score: " <<
	i.value() << std::endl;
    }

    return 0;
}

